/*
 * Level2 Ember Gulp Plugin
 *   Compiles files from Gulp via the Level2 Ember Compiler
 *
 */
var l2Ember = require('level2-ember'),
	File = require('vinyl'),
	path = require('path'),
	through2 = require('through2');

module.exports = function(options) {
	options = options || {};
	
	// Merge options
	options.debugMode = options.debugMode === false ? false : true;
	options.dependencyPath = options.dependencyPath || null;
	options.templateCompiler = options.templateCompiler || null;
	
	// Setup the compiler
	var compiler = new l2Ember.Compiler({
			debug: options.debugMode,
			templateCompiler: options.templateCompiler
		}),
		compileBuffer = [];
	compiler.on('data', function(chunk) {
		compileBuffer.push(chunk);
	});
	
	// Setup plugin stream
	var pluginStream = through2.obj(
		
		// Compile the file
		function(file, encoding, callback) {
			var projPath = file.path.replace(file.base, '');
			
			// Check for app.js
			if(/app\.js$/.exec(file.path) !== null) {
				compiler.addApp(file.contents, encoding);
				return callback();
			}
			
			// Check for javascript
			if(/\.js$/.exec(file.path) !== null) {
				compiler.addSource(file.contents, encoding, projPath);
				return callback();
			}
			
			// Check for template
			var normalizedPath = file.path.split(path.sep).join('/'),
				templateMatch = /templates\/.+\.hbs/.exec(normalizedPath);
			if(templateMatch !== null) {
				compiler.addTemplate(templateMatch[0], file.contents, encoding);
				return callback();
			}
			
			// Throw error, unknown file
			callback(new Error('Unsupported file: ' + file.path));
		},
		
		// Save the output
		function(callback) {
			var self = this;
			
			compiler.on('end', function() {
				self.push(new File({
					path: 'output.js',
					contents: Buffer.concat(compileBuffer)
				}));
				callback();
			});
			
			// Add dependencies and finalize
			if(!!options.dependencyPath) {
				compiler.addDependencies(options.dependencyPath).then(function() {
					compiler.finalize();
				}, callback);
			} else {
				compiler.finalize();
			}
		}
		
	);
	
	// Handle compile errors
	compiler.on('error', function(err) {
		pluginStream.emit('error', err);
	});
	
	return pluginStream;
};
