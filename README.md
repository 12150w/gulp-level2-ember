# Level2 Ember Gulp Plugin
This plugin exposes the Level2 Ember compiler to Gulp.



## Usage

Simply pipe files to the plugin to compile them.
The plugin will output 1 file containing the compiled project.

```javascript
var gulp = require('gulp'),
	compile = require('gulp-level2-ember');

gulp.task('compile', function() {
	return gulp.src('path/to/project')
		.pipe(compile())
		.pipe('path/to/output');
});
```

The module accepts options as the first argument when constructing the compiler.

```javascript
var gulp = require('gulp'),
	compile = require('gulp-level2-ember');

gulp.task('compile', function() {
	return gulp.src('path/to/project')
		.pipe(compile({ debugMode: false }))
		.pipe('path/to/output');
});
```



## Options

* **[debugMode]** `Boolean`: Enables debug mode so that compiled sources are isolated in the
	debugger (default `true`)
* **[dependencyPath]** `String`: The path to dependencies to be loaded by the compiler
* **[templateCompiler]** `String`: Path to the ember template compiler to use
