/*
 * Unit Tests
 *   Using the mocha framework
 *
 */
var compileEmber = require('./plugin'),
    es = require('event-stream'),
    assert = require('assert'),
    File = require('vinyl');
    
describe('Level2 Ember Compiler', function() {
    var appFile, appComment;
    
    beforeEach('Create the app file', function() {
        appComment = '//APP-' + (new Date()).getTime();
        appFile = new File({
            base: '/',
            path: '/app.js',
            contents: new Buffer(appComment, 'utf8')
        });
    });
    
    it('outputs the compiled project', function(done) {
        var compileStream = compileEmber();
        
        compileStream.write(appFile);
        
        compileStream.once('data', function(output) {
            assert.ok(output.path.indexOf('output.js') > -1);
            assert.ok(output.contents.toString('utf8').indexOf(appComment) > -1);
            done();
        });
        
        compileStream.end();
    });
    
});
